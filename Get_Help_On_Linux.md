# Get help with `man`

`man command`  => to get the manuel page of the command

- Man has 8 standard sections: 

    - *1* is for the shell commands
    - *5* is for File formats and conventions (Config file)
    - *8* System administration commands

- Used `man -s 8` to get manuel page for System administration

- Used `/` in man output to search word & `n` tp movoe to another


**Finding the Right man page**

- `man` pages are summarized in the `mandb`

- Used `man -k or apropos` to search the `mandb`

- Used `sudo mandb` to update

# Get help using `pinfo`

- Get the right '`pinfo` command at the end of `man pages`

- eg: at the end of `man ls`  =>  `pinfo '(coreutils) ls invocation'`

# Using `tldr`