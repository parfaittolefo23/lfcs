# Linux file system

- `man hier`  => Show the FHS

# Using wildcard

Refer to: [gutsytechster](https://gutsytechster.wordpress.com/2018/02/06/wildcardsfilters-and-regex-in-linux/)

# Understanding Links

- A link is a file system entry that refers to another file or directory

- Hard links are pointing to the same inode on the same file system
    - Only used with file ( Not with a directory)

- Symbolic links are schotcuts and add additional flexibility
    1- Symbolic links can exist on a directory
    2- Cross-device symbolic links can be used

- `ln existing_file hardlin_file` => Create hardlink to  existing_file

- `ln -s existing_file/dir symblinc existing_file/dir` => Create symbolic link to  existing_file/dir

# Finding file using `find` command

- `find / -name "host"` 

- `find / -user linda`

- `find / -size +2G`

- `find / -type f -name *.txt -printf '%s %p\n'` => **%s** for size & **%p** for the filename

- `find / -perm /4000`

- `find / -user linda -exec grep -l student {} \; -exec cp {} /tmp/student/ \;`

- `find /etc -name '*' -type f | xargs grep '127.0.0.1'`

- `find / -name 'student' -type f! -path '*/proc/*'! -path '*/tmp/*`

# Using `wich` and `locate`

- `wich binary.sh` => find the binary.sh from $PATH variable

- `locate file` to find the location of the *file* base on its DB

- `sudo updatedb`  => update *locate DB*

# Using `tar` fir archive

- `tar -cvf mytar.tar /home`

- `tar -xvf mytar.tar` or `tar -xvf mytar.tar -C /tmp`

- `tar -tvf mytar.tar` => show mytar.tar content

- Compresion mode: **-z**, **-j**, **-J**

# Managing mounts

- `lsblk` => lists block devices

- `mount` => list all current mounts (administrative mounts included)

- `df -h` => present mounted devices, including available disk space

- `findmnt` => show all mounts

- `sudo mount /dev/sdb /mtn` => mount /dev/sdb on /mnt

- `sudo umount /mnt` => to umount the device /dev/sdb on /mnt

- `df -h` => **d**isk **f**ree 

- `blkid` => id of block useful for automatique mounted config

- `/etc/fstab` for automatique mounted config

