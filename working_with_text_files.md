# Work with test file using `vim` 

- After starting `vim`, it opens in *command mode*

- Use **i** (Instert) or **o** (open new line) or **a** (append) to get into insert mode

- `ESC` to get back to command mode

    - `:wq!` ==> write, quit and don't complain

    - `u` ==> undo the last change

    - `Ctrl-r` ==> Redo

    - `dd` ==> delete the current line

    - `v` ==> visual mode

    - `3dd` ==> delete the 3 lines start from current posistion

    - `y` ==> copy(yank) the current selection

    - `p` ==> past the copied lines

    - `O/o` ==> open line *above/after* cusor position

    - `w/b` ==> Go to next word/ back word, cab be used like `5w/b`

    - `^/$` ==> Move to *start/end* of current line 

    - `:5` ==> show line numbers 5

    - `dw` ==> delete the current word

    - `/text` ==> search text

    - `gg` ==> Move the cursor to the top 

    - `G` move the cursor to the end

    - `r` ==> replace current char

    - `:%s/old/new/g` ==> replaces all occurrence of **old** with **new**

- `vimtutor` ==> For using tuto on system


# Using `grep` command

- `-i` ==> ignore case

- `-v` ==> Show all lines that do not contain pattern

- `-l` ==> List only file that content parttern without showing lies

- `-A5` ==> Show lines that matches pattern as well as 5 lines after

- `-B5` ==> Show lines that matches pattern as well as 5 lines before

- `-C5` ==> Combined -A5 and -B5 

- `-R`  ==> Recursively search

# `cut` and `sort`  command

- `cut -d :`  ==> specify delimiter `:`

- `cut -f 1`  ==> specify the column `1`

- `sort` ==> sort the output

-`sort -n` ==> sort in numeric order 

# `tr` command

- `tr`  translates sets of characters into others sets of characters

- Useful for converting upper into lower case or the other way around

- eg
    - `echo hello | tr [:lower:][:upper:]`  ==> convert to upper case

    - `echo hello | tr [a-z][A-Z]`          ==> convert to upper case

    - `echo hello | tr [a-m][n-z]`


# `awk` command

- eg

    - `awk '{ print $0 }' /etc/passwd`   ==> Print entier line 

    - `awk 'lenght($0)>40' /etc/passwd`  ==> Print entire line with char more that 40

    - `awk -F : '{ print $4 }>40'/etc/passwd`      ==> Print 4th field of the line 

    - `awk -F : '/user/{ print $4 }' /etc/passwd`  ==> Print 4th field of the line content *user*



# `sed` command

- eg

    - `sed -n 5p /etc/passwd` ==> Print line 5

    - `sed -i s/old/new/g ~/file` ==> Substitute all occurrence of *old* by *new*  in *file*

    - `sed -i -e '2d' ~/file`    ==> delete line 2 in *file*

    - `for i in *conf; do sed -i 's/old/new/g' $i; done`

# Regular expressions

- Always put your regex between single quotes !!!

- `man 7 regex`  ==>  to know more

![Regex summary expressions](image.png)

