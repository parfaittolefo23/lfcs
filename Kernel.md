**The kernel is the first part of system loaded when we power on a system.**

- Kernel interact with the hardwares throught the drivers as modules
- Systemd-udevd is used to load dynamically the kernel files while needed

**Usual module commands**

- `lsmod` => Show all module currently loaded 

- `modprobe` => Used to manually load mokernel files with its depandencies

- `modprobe -r` => Used to manually unload kernel files

- `modinfo` => Provides information about kernel modules

- `depmod` => Generates the modules.dep kernel module dependencies file

- Kernel module parameters as listed by `modinfo` can be specified in included files in `/etc/modprobe.d`
 
 
 **Kernel Tuning/Kernel optimization**

 - _/proc_ pseudo filesystem is used for kernel tuning and optimization

 - Used `echo 60> /proc/sys/vm/swappiness` to change the  swapinness value *(Not persistent)*
 
 - Used `man parameter` => `man swappiness` to get help about the parameter

 - Used `sysctl` for persistent config 

- Parameters used in these files are relative filenames of filles in _/proc/sys_

- `/proc/sys/vm/swappiness` becomes `vm.swappiness`

 **Persistent Config Example**
- `sysctl -a` => show all sysctl parameters

- `echo vm.swappiness=70 > /etc/sysctl.d/swappiness.conf` => change the value  

- `sysctl -p /etc/sysctl.d/swappiness.conf`  => to load the config

- `sysctl -a | grep swappiness`



