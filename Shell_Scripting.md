# Basics
- Start a Script with `shebang` => `#!/bin/bash`

- Code within the script is executed in a subshell

- To run the script in the curent shell, use `source script.sh` or `. script.sh `

- Get argument with $n 

- read to get information from input 

- To get help for condition, use `man test`

# Pattern matching

$1=/usr/bin/blah

- ${#1} => Print the string lenght of $1                            `${#1}`    ==> _13_

- ${1#string} removes the shortest match from the front end of $1   `${1#*/}`  ==> _usr/bin/blah_

- ${1##string} removes the longest match from the front end of $1  `${1##*/}`  ==> _blah_

- ${1%string} removes the shortest match from the back end of $1   `${1%/*}`   ==> _/usr/bin_

- ${1%%string} removes the longest match from the back end of $1    `${1%%b*}` ==> _/usr/_