**Systemd is the utility to manages services on Linux**

# Editing Systemd Units

- `systemctl show unit.type`  **eg:** `systemclt show sshd.service` => shows possible Unit parameters

- `systemctl edit unit.type` => Change settings for units

- `systemctl daemon-reload` => to update systemd config

- `systemctl restart unit.type` => to restart the unit with news settings

- Used `man systemd.directives` to know about the system unit settings

- You will get other _man page_ eg: `man systemd.resource-control`

- `systemctl cat  unit.type` => Read actual config

# Sockets

- A systemd socket is used with a service to start the service when traffic comes in on a specific socket

- The service and the socket files need to have the same name

- Used **ListenStream** to listen on *TCP*

- Used **ListenDatagram** to listen on *UDP*

- When using sockets, the socket is _started/enabled_ and not a service

- `socket list-unit-files [ -t {socket | service | timer ...} ` => liste all unit base on the **-t** parameter

- `systemctl enable --now unit.socket` *eg:* `systemctl enable --now cockpit.socket` => To enable cockpit.socket  

# Managing Systemd Timers

- A systemd timer is used to start the corresponding service at a specific time, or on occurence of a specific event 

- The timer and service need to have the same filename

- Used `OnCalendar` to specify when the timer should be started in cron-like style

- Used `OnBootSec` or `OnUnitActiveSec` to run the timer based on other event

- When using timers, the timer is **enabled/started** not the service

- Used `man 7 systemd.time` for more informations about the *time* config and so one

- `man 7 systemd.timer` to get manuel page of *systemd timer* config

# Cgroups

-
-
-

# Unit dependency management

**Different startments can be used in the `[Unit]` section to define unit file dependencies.**

- **before**: this unit starts before the specified unit

- **after**: this unit starts when the specified unit started

- **requires**: When this unit starts, the unit listed here is also started. If the specified unit fails, this one also fails

- **wanted**: when this unit starts, the unit listed here is also started 


# Configuring Self-Healing

**Restart automically the service**
 
    [Service]
    Restart=always
    RestartSec=3

- `systemctf daemon-reload`

- `systemctl restart unit.type`