# Network interface configuration,  *not persistent*

- `ip link show` => to get available interfaces

- `lshw -class network` => For more in-depth

- `ip addr add` =>to configure interface, not persistent

    - *eg*: `ip addr add dev ens33 10.0.0.1/24`

- `ethtool` => change device setting such as **auto-negociation**, **port speed** and more 


# Network interface configuration,  *persistent*

- `NetworkManager` is used for persistent interface config on *Red Hat Family Linux and indirectly on Ubuntu Desktop*

    - `nmtui` or 
    - `nmcli` to manage

- On **Unbuntu Server** as well **Desktop**, **Netplan** is used for persistent interface config

    - `netplan`  provide YAML configuration abstraction for various back-ends

    - Based on this YAML file, back-end specific configuration files are generated 

    - On ubuntu Server, `systemd-networkd` is used as the default back-end to `netplan`

    - It uses one or more YAML files, provided by Netplan for the network config: **/etc/netplan/*.yaml**
    
    - **renderer:networkd** is used to connect to **systemd-networkd**

    - On the Ubuntu Desktop, **NetworkManager** is used as the default back-end

- The front-end for querying network status is `networkctl`

    - `networkctl status`

    - `networkctl up | down`


# 1- Netplan Static IP Configuration (Ubuntu)

- Create configuration file as `/ect/netplan/99_cinfig.yaml`

- eg:

        network:
        version: 2
        renderer: networkd
        ethernets:
        eth0:
        addresses:
            - 192.168.1.105/24
        gateway4: 192.168.1.104
        nameservers:
            search: [domain1, domaine2]
            addresses: [192.168.1.104, 8.8.8.8]


# 2- NetworkManager Static IP Configuration (Red Hat family => CentOS)

- Using `mntui` **Recommanded for the exam** 
    - Edit the configuration **under manual**
    
    - Then go to **activate** press **desactivate/activate** to active the conf

- Using `nmcli`

    - `nmcli` the use `tab key` to select option
    - `man nmcli-examples` => print the axemples


# Managing Static Routes, *Non-Persistent*

- `ip route show`  => Show the default gateway

- `ip route add 192.2.0.0/24 via 10.0.0.10` => Add new route to  192.2.0.0/24 via 10.0.0.10 **Non-persistent**

- `ip route add 192.2.0.0/24 via 10.0.0.10 dev ens33` => Add new route to  192.2.0.0/24 via 10.0.0.10 on interface **ens33** **Non-persistent**

# Managing Static Routes, *Persistent*

# 1- On the Ubuntu Netplan

    - The routes configuation is a property of the network car in **/etc/netplan/*.yaml** file

        network:
         version: 2
         renderer: networkd
         ethernets:
          eth0:
           dhcp4: true
           routes:
            -to: 192.168.22.0/24
            via: 192.168.0.1
        
    
# 2- On the Red Hat family

- The best way is by using `nmcli connection modify connection-name+ipv4.routes`

- eg

    - `nmcli con modify eth0 +ipv4.routes "192.2.0.0/24 10.0.0.10"`
    - `nmcli con down eth0` => down the interface 
    - `nmcli con up eth0` => up the intercafe to allow the new config

- This configuration is writed under */etc/NetworkManager/system-connections

- We can use also `mntui`

# Manage Hostname and DNS conf

- DNS is commonly used for hostname resolving

- _/etc/hosts_ is used to maintain local list of hostnames matching IP addresses

- How hostname resolving is used, is defined in _/etc/nsswitch.conf_, it is used to define the order in which **hostanme** resolution should happen:
 
    - eg: _hosts: files dns_

- */etc/resolv.conf* is the old standard for configuration DNS _nameservers_

- On the **NetworkManager** systems, the file can be written directly or is managed by NetworkManager 

- On the **systemd-networkd** systems a different approch iused
    
    - The file is managed with **systemd-resolved**

    - **/etc/resolv.conf** is a simbolic link to **/run/systemd/resolve/stub-resolv.conf**

# Setting the localhostname

- The local hostname is known to the kernel through **/proc/sys/kernel/hostname**

- `hostnamectl` =>  Used to manage hostname

- Most distribution maintain the **/etc/hostname** config file, which is managed by `hostnamectl` and from wich the hostname is obtained by the kernel

- `hostnamectl hostname myos.local.com`  => to set the hostname