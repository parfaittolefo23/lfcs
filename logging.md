# How logging works on linux

There are two system for logginf

- *systemd-journald* ==> The systemd-integrated logging

- *rsyslog* ==> The laegacy Linux logging system

**Diff**: *rsyslog* offer some feature that *systemd-journald* not offering

`journalctl`  ==> Check all message generate my system unit

These are not persistent by default

# 1- rsyslogd 

- rsyslogd service is compatible with the legacy syslogd service

- It uses *facility*: *severity destination* to ensure that message are written to the appropriate location 

- The main conf file is **/etc/rsyslog.conf/**

- `systemctf restart rsyslog.service`  ==> to reload after any conf change

- So *facilities* and *priorities* are used to define how logging should happpened

- **Facilities** define the items for which logging happening

- **Priotities** define the severity levels in wich case messages should be logged
 
- Typically, messages are written in */var/log* but output module can be used

- Module can be used to handle log input and log output


# 2- systemd-journald

- `journalctl` ==> to get the journal 

- `journalctl -u unit.type`  ==> to inverstigate specific unit

- When in **/etc/systemd/journald.conf/** the option `Storage=auto` is set, a 
persistent journal will be created after creating a directory **/var/log/jpournal**

- After this, r*reboot* the system or use `systemctl restart systemd-journal-flush.service`
 

# Manage Logrotate

- Logrotate is mechanism that ensures thet log files don't grow too big

- Logrotate is started as a scheduled job throungh cron or systemd timer, and work with the */etc/logrotate.conf*  and */etc/logrotate.d* conf files